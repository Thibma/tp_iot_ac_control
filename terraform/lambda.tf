# aws_lambda_function to control air conditioner

# aws_cloudwatch_event_rule scheduled action every minute

# aws_cloudwatch_event_target to link the schedule event and the lambda function

# aws_lambda_permission to allow CloudWatch (event) to call the lambda function

resource "aws_lambda_function" "test_lambda" {
  # If the file is not in the current working directory you will need to include a 
  # path.module in the filename.
  filename      = "files/empty_package.zip"
  function_name = "ac_control_lambda"
  role          = aws_iam_role.lambda_role.arn
  handler       = "ac_control_lambda.lambda_handler"

  runtime = "python3.7"
}

resource "aws_cloudwatch_event_rule" "console" {
  name        = "capture-aws-sign-in"
  schedule_expression = "rate(1 minute)"
}

resource "aws_cloudwatch_event_target" "lambda" {
  target_id = "lambda"
  rule      = aws_cloudwatch_event_rule.console.name
  arn       = aws_lambda_function.test_lambda.arn
}

resource "aws_lambda_permission" "allow_cloudwatch" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.test_lambda.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.console.arn
}