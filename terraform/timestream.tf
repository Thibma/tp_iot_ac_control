# aws_timestreamwrite_database

# aws_timestreamwrite_table linked to the database

resource "aws_timestreamwrite_database" "iot" {
  database_name = "iot"
}

resource "aws_timestreamwrite_table" "example" {
  database_name = aws_timestreamwrite_database.iot.database_name
  table_name    = "temperaturesensor"
}

