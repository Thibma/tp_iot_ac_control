# aws_iot_certificate cert

# aws_iot_policy pub-sub

# aws_iot_policy_attachment attachment

# aws_iot_thing temp_sensor

# aws_iot_thing_principal_attachment thing_attachment

# data aws_iot_endpoint to retrieve the endpoint to call in simulation.py

# aws_iot_topic_rule rule for sending invalid data to DynamoDB

# aws_iot_topic_rule rule for sending valid data to Timestream


###########################################################################################
# Enable the following resource to enable logging for IoT Core (helps debug)
###########################################################################################

#resource "aws_iot_logging_options" "logging_option" {
#  default_log_level = "WARN"
#  role_arn          = aws_iam_role.iot_role.arn
#}

resource "aws_iot_certificate" "cert" {
  active = true
}

resource "aws_iot_policy" "pubsub" {
  name = "PubSubToAnyTopic"

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  policy = file("files/iot_policy.json")
}

resource "aws_iot_policy_attachment" "att" {
  policy = aws_iot_policy.pubsub.name
  target = aws_iot_certificate.cert.arn
}

resource "aws_iot_thing" "sensor" {
  name = "sensor"
}

resource "aws_iot_thing_principal_attachment" "att" {
  principal = aws_iot_certificate.cert.arn
  thing     = aws_iot_thing.sensor.name
}

data "aws_iot_endpoint" "endpoint" {
    endpoint_type = "iot:Data-ATS"
}

resource "aws_dynamodb_table" "example" {
  name             = "Temperature"
  hash_key         = "Id"
  read_capacity  = 20
  write_capacity = 20

  attribute {
    name = "Id"
    type = "S"
  }
}

resource "aws_iot_topic_rule" "rule" {
  name = "SQL"
  sql = "SELECT *  FROM 'sensor/temperature/+' where temperature >= 40"
  enabled = true
  sql_version = "2016-03-23"

  timestream {
    database_name = aws_timestreamwrite_database.iot.database_name
    table_name = aws_timestreamwrite_table.example.table_name

    dimension {
      name  = "sensor_id"
      value = "$${sensor_id}"
    }

    dimension {
      name  = "temperature"
      value = "$${temperature}"
    }

    dimension {
      name  = "zone_id"
      value = "$${zone_id}"
    }

    timestamp {
      unit  = "MILLISECONDS"
      value = "$${timestamp()}"
    }
    role_arn = aws_iam_role.role.arn
  }

  dynamodbv2 {
    put_item {
      table_name = aws_dynamodb_table.example.name
    }
    role_arn = aws_iam_role.role.arn
  }
}

resource "aws_iam_role" "role" {
  name = "myrole"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "iot.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "test_policy" {
  name = "test_policy"
  role = aws_iam_role.role.id

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "dynamodb:PutItem",
        ]
        Effect   = "Allow"
        Resource = "*"
      },
    ]
  })
}